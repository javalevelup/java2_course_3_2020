package ru.levelup.lessons.reporistory.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.levelup.musicians.library.model.Bank;
import ru.levelup.musicians.library.repository.impl.HibernateBankRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class HibernateBankRepositoryTest {

//    private SessionFactory factory = new SessionFactoryStub();
//    private HibernateBankRepository repository = new HibernateBankRepository(factory);

    private SessionFactory factory;
    private Session session;
    private Transaction transaction;

    private HibernateBankRepository repository;

    // @BeforeAll << метод вызывается только один раз перед начало тестов
    // public static void setup() {}
    // @AfterAll
    // @AfterEach

    @BeforeEach // << этот метод вызывается каждый раз перед @Test методом
    public void setupTest() {
        factory = mock(SessionFactory.class); // заглушка для класса SessionFactory
        session = mock(Session.class);
        transaction = mock(Transaction.class);

        // Mockito.when(a.method(1)).thenReturn(100);
        // a.method(2); >> a.method will return 0

        when(factory.openSession()).thenReturn(session);
        when(session.beginTransaction()).thenReturn(transaction);

        repository = new HibernateBankRepository(factory);
    }

    @Test
    public void testCreateBank() {
        // given
        // when
        Bank result = repository.createBank("VTB24");
        // then
        // assertNotNull(result.getId());
        assertEquals("VTB24", result.getName());

        verify(transaction).commit();  // проверяет, что один раз был вызван метод transaction.commit
        verify(transaction, times(0)).rollback();
        verify(session).close();
    }

    @Test
    public void testFindAllBanks() {
        // given
        Query<Bank> query = mock(Query.class);
        when(session.createQuery("from Bank", Bank.class)).thenReturn(query);

        List<Bank> expectedResult = new ArrayList<>();
        expectedResult.add(new Bank("VTB24"));
        expectedResult.add(new Bank("SberBank"));

        when(query.getResultList()).thenReturn(expectedResult);

        // when
        List<Bank> result = repository.findAllBanks();

        // then
        for (int idx = 0; idx < expectedResult.size(); idx++) {
            assertEquals(expectedResult.get(idx), result.get(idx));
        }

        verify(session).close();
    }

}
