package ru.levelup.musicians.library.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "banks")
public class Bank {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @ManyToMany(mappedBy = "banks")// << поле из класса Person
//    @JoinTable(
//            name = "bank_persons",
//            inverseJoinColumns = @JoinColumn(name = "person_id"), // имя колонки из таблицы bank_persons, которая является внешним ключем на текущую таблицу (на таблицу Persons)
//            joinColumns = @JoinColumn(name = "bank_id")
//    )
    private List<Person> bankPersons;

    public Bank() {
        this.bankPersons = new ArrayList<>();
        // Bank bank = new Bank();
        // bank.getBankPersons().add(person);
    }

    public Bank(String name) {
        this.name = name;
    }

}
