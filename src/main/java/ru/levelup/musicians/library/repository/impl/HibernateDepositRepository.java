package ru.levelup.musicians.library.repository.impl;

import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import ru.levelup.musicians.library.model.Deposit;
import ru.levelup.musicians.library.repository.DepositRepository;

@RequiredArgsConstructor
public class HibernateDepositRepository implements DepositRepository {

    private final SessionFactory factory;

    // public HibernateDepositRepository(SessionFactory factory) { this.factory = factory; }

    @Override
    public Deposit createDeposit(String name, double rate, boolean canPartialRemoval, boolean canReplenishment, double minAmount) {
        // ACID
        // A - аторманость - все операции должны выполниться или база должна откатиться до начального состояни
        // C - consistency (согласованность данных) - нельзя фиксировать изменения, которые не удовлетворяют constraint
        // I - isolation (изоляция)
        // D - durability - если коммит выполнен, то данные находятся в базе
        Transaction tx = null;
        try (Session session = factory.openSession()) {
            tx = session.beginTransaction();

            Deposit deposit = new Deposit();
            deposit.setName(name);
            deposit.setRate(rate);
            deposit.setCanPartialRemoval(canPartialRemoval);
            deposit.setCanReplenishment(canReplenishment);
            deposit.setAmount(minAmount);

            session.persist(deposit); // << void persist(object); -> object.id = <generated id>

            System.out.println("Generated deposit ID: " + deposit.getId());

            tx.commit(); // фиксирует все изменения, которые были сделаны в транзакции

            return deposit;
        } catch (Exception exc) {
            if (tx != null) {
                tx.rollback(); // откат всех изменения до начала транзакции
            }
            throw new RuntimeException(exc);
        }
    }

}
