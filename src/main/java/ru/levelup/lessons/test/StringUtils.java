package ru.levelup.lessons.test;

public class StringUtils {

    public static boolean isEmpty(String value) {
        // true:
        //      -> value = null
        //      -> value = ""
        //      -> value = "     "
        return value == null || value.trim().isEmpty();
    }

    public static void requiredNotEmpty(String value) {
        if (isEmpty(value)) {
            throw new IllegalArgumentException("Value must be not empty");
        }
    }

}