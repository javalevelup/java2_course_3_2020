package ru.levelup.lessons.reflection.annotation;

import lombok.SneakyThrows;
import ru.levelup.lessons.reflection.Watch;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.Random;

public class RandomIntAnnotationProcessor {

    @SneakyThrows
    public Watch initializeWatch() {
        // Создать объект класса Watch
        // Найти поля, у которых есть аннотация @RandomInt
        // Сгенерировать значение
        // Засетить значение в поле
        Class<?> watchClass = Watch.class;

        Constructor<?> emptyConstructor = watchClass.getDeclaredConstructor();
        Watch watchObj = (Watch) emptyConstructor.newInstance(); // << создали новый объект класса Watch

        Field[] fields = watchClass.getDeclaredFields(); // << получаем все поля класса Watch
        for (Field f : fields) {
            RandomInt annotation = f.getAnnotation(RandomInt.class);
            if (annotation != null) { // << это означает, что над полем есть аннотация
                int minValue = annotation.min(); // annotation.min;
                int maxValue = annotation.max();

                Random r = new Random();
                int randomValue = r.nextInt(maxValue - minValue) + minValue;

                f.setAccessible(true);
                f.set(watchObj, randomValue);
            }
        }

        return watchObj;
    }

}
