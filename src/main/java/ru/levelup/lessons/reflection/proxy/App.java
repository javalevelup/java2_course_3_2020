package ru.levelup.lessons.reflection.proxy;

public class App {

    public static void main(String[] args) {
        ServiceFactory factory = new ServiceFactory();
        LoginService loginService = factory.createLoginService();

        loginService.login("log", "pass");
        loginService.logout("log");

    }

}
