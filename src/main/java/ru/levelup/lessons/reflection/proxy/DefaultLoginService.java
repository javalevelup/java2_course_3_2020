package ru.levelup.lessons.reflection.proxy;

public class DefaultLoginService implements LoginService {

    @Override
    @LogTimeExecution
    public void login(String login, String password) {
        System.out.println(String.format("Verify login %s and password %s", login, password));
    }

    @Override
    public void logout(String login) {
        System.out.println(String.format("Success logout %s", login));
    }

}
