package ru.levelup.lessons.stream;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Streams {

    public static void main(String[] args) {

        Collection<Task> tasks = new ArrayList<>() {{
            add(new Task("1", "Title 1", LocalDate.of(2019, 12, 1), false, new HashSet<>(), null));
            add(new Task("2", "Title 2", LocalDate.of(2019, 11, 10), false, new HashSet<>(), null));
            add(new Task("3", "Title 3", LocalDate.of(2020, 12, 17), false, new HashSet<>(), null));
            add(new Task("4", "Title 4", LocalDate.of(2020, 4, 15), false, new HashSet<>(), null));
        }};

        List<Task> sorted = tasks.stream()
                // .sorted(new TaskCreatedDateComparator())
                // .sorted(Comparator.comparing(task -> task.getCreatedOn()))
                // .sorted(Comparator.comparing(Task::getCreatedOn))
                // коллекцию преобразуем в коллекцию, состоящию из полей createdOn, и потом ее сортируем
                .sorted(Comparator.comparing(Task::getCreatedOn, (o1, o2) -> -o1.compareTo(o2)))
                // .sorted(Comparator.comparing(Task::getCreatedOn, (o1, o2) -> -o1.compareTo(o2)))
                .collect(Collectors.toList());

        // sorted.forEach(value -> System.out.println(value));
        sorted.forEach(System.out::println);

        // method(Consumer<?> consumer)
        // .method(value -> invokeMethod(value)); -> object::invokeMethod

//        String allInOne2 = "";
//        tasks.stream().forEach(task -> allInOne2+= task.getTitle()+"***");

        // fail-fast vs fail-save
        LinkedList<Task> list = new LinkedList<>(tasks);
        Iterator<Task> iterator = list.iterator();
        while (iterator.hasNext()) {
            iterator.remove();
        }
//        for (Task t : list) { // ->> Iterator<Task>
//            if (t.getId().equals("1")) {
//                list.addFirst(t); // >> ConcurrentModificationException (CME)
//            }
//        }

    }

    static class TaskCreatedDateComparator implements Comparator<Task> {
        @Override
        public int compare(Task o1, Task o2) {
            return o1.getCreatedOn().compareTo(o2.getCreatedOn());
        }
    }

}
