package ru.levelup.lessons.stream;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.time.LocalDate;
import java.util.Set;

@ToString
@AllArgsConstructor
@Getter
public class Task implements Comparable<Task> {
    private final String id;
    private final String title;
    private final LocalDate createdOn;
    private final boolean done;
    private final Set<String> tags;
    private final LocalDate dueOn;

    @Override
    public int compareTo(Task o) {
        return 0;
    }

}

