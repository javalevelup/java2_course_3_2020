package ru.levelup.lessons.threads;

public class Threads {

    public static void main(String[] args) throws InterruptedException {
        // class extends Thread
        // class implements Runnable
        // class implements Callable

        Thread t1 = new CounterThread();
        Thread t2 = new Thread(new CounterRunnable());
        Thread t3 = new Thread(() -> {
            try {
                System.out.println("Daemon thread");
                Thread.sleep(1000);     // остановить (или усыпить) поток на 3секунды
                System.out.println("Daemon thread is dead");
            } catch (InterruptedException exc) {
                System.out.println("Delete daemon thread");
            }
        });
        t3.setDaemon(true);

        t1.start();
        t2.start();
        t3.start();

        new Thread(() -> {
            // run method
            try {
                t1.join();
                System.out.println("Анонимный поток");
            } catch (InterruptedException exc) {
                throw new RuntimeException(exc);
            }
        }).start();

        t1.join(); // поток main должен дождаться выполнения потока t1
        t2.join();

        // t1.run();
        // t2.run();
        System.out.println("End of Main thread");
    }

    static class CounterThread extends Thread {
        @Override
        public void run() {
            for (int i = 0; i < 15; i++) {
                System.out.println("CounterThread: " + i);
            }
        }
    }

    static class CounterRunnable implements Runnable {
        @Override
        public void run() {
            for (int i = 0; i < 15; i++) {
                System.out.println("CounterRunnable: " + i);
            }
        }
    }

}
