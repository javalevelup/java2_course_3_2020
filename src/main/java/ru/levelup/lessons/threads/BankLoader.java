package ru.levelup.lessons.threads;

import lombok.RequiredArgsConstructor;
import ru.levelup.musicians.library.model.Bank;
import ru.levelup.musicians.library.repository.BankRepository;

import java.util.List;

@RequiredArgsConstructor
public class BankLoader implements Runnable {

    private final BankRepository bankRepository;

    @Override
    public void run() {
        // each 2 seconds display banks
//        while (!Thread.currentThread().isInterrupted()) { // for(;;)
        while (true) { // for(;;)
            // System.out.println("Статус потока: " + Thread.currentThread().isInterrupted());
            System.out.println(Thread.interrupted());
            System.out.println(Thread.interrupted());

            List<Bank> allBanks = bankRepository.findAllBanks();
            System.out.println("Список банков:");
            // allBanks.forEach(bank -> System.out.println(bank.getName()));
            System.out.println();
//
//            try {
//                Thread.sleep(2000); // currentThread().sleep();
//            } catch (InterruptedException exc) {
//                System.out.println("Поток был прерван");
//
//            }
        }

    }

}
