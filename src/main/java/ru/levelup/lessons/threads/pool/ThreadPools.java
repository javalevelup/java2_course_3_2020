package ru.levelup.lessons.threads.pool;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ThreadPools {

    public static void main(String[] args) {
//        ExecutorService executorService = Executors.newSingleThreadExecutor();
//        ExecutorService executorService = Executors.newFixedThreadPool(3);
        ExecutorService executorService = Executors.newCachedThreadPool(); // 0 потоков в начале
        executorService.execute(() -> {
            System.out.println("Another thread");
        });

        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(4);
        //scheduledExecutorService.schedule(() -> System.out.println("Scheduled message"), 4, TimeUnit.SECONDS);
        // scheduledExecutorService.scheduleWithFixedDelay(() -> {});
        scheduledExecutorService.scheduleAtFixedRate(() -> {
            System.out.println("Start: " + new Date());
            try {
                Thread.sleep(5000);
                System.out.println("End: " + new Date());
            } catch (InterruptedException exc) {}
        }, 0, 2, TimeUnit.SECONDS);

        // Executors.newSingleThreadScheduledExecutor()

        executorService.shutdown();
        // executorService.shutdown();
        scheduledExecutorService.shutdown();
    }

}
