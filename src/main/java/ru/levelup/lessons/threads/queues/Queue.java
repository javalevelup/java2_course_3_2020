package ru.levelup.lessons.threads.queues;

public interface Queue<T> {

    void put(T task);

    T take();

}
