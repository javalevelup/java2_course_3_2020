package ru.levelup.lessons.threads.queues;

import lombok.SneakyThrows;

public class ConsumerThread extends Thread {

    private final Queue<String> taskQueue;
    private final ShutdownFlag flag;
    // private boolean shutdownFlag;

    public ConsumerThread(int consumerNumber, Queue<String> taskQueue, ShutdownFlag flag) {
        super("Consumer-" + consumerNumber);
        this.taskQueue = taskQueue;
        this.flag = flag;
        // this.shutdownFlag = false;
    }

    @Override
    @SneakyThrows
    public void run() {
        while (!flag.isShutdown()) {
            String task = taskQueue.take();
            System.out.println(getName() + " получил задачу " + task);
            // Thread.sleep(250);
        }
        System.out.println(getName() + " завершил работу");
    }

//    public void shutdown() {
//        shutdownFlag = true;
//    }

}
