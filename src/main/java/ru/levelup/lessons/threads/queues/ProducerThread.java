package ru.levelup.lessons.threads.queues;

import lombok.SneakyThrows;

public class ProducerThread extends Thread {

    private final Queue<String> queue;
    private final int startNumber;

    public ProducerThread(int producerNumber, int startNumber, Queue<String> queue) {
        super("Producer-" + producerNumber);
        this.startNumber = startNumber;
        this.queue = queue;
    }

    @Override
    @SneakyThrows
    public void run() {
        for (int i = startNumber; i < startNumber + 15; i++) {
            System.out.println(getName() + " сгенерировал задачу " + i);
            queue.put(String.valueOf(i));
            Thread.sleep(200);
        }
        System.out.println(getName() + " завершил работу");
    }
}
