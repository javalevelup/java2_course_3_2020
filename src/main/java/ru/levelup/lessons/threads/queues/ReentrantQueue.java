package ru.levelup.lessons.threads.queues;

import lombok.SneakyThrows;

import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class ReentrantQueue<T> implements Queue<T> {

    private final LinkedList<T> queue;
    private final int size;

    private final ReentrantLock lock;
    private final Condition emptyQueue;
    private final Condition fullQueue;

    public ReentrantQueue(int size) {
        this.size = size;
        this.queue = new LinkedList<>();

        this.lock = new ReentrantLock();
        this.emptyQueue = lock.newCondition();
        this.fullQueue = lock.newCondition();
    }

    @Override
    @SneakyThrows
    public void put(T task) {
        lock.lock();
        try {
            while (size == queue.size()) {
                fullQueue.await();
            }

            queue.addLast(task);
            emptyQueue.signalAll();
        } finally {
            lock.unlock();
        }
    }

    @Override
    @SneakyThrows
    public T take() {
        lock.lock();
        try {
            while (queue.isEmpty()) {
                emptyQueue.await();
                // emptyQueue.await(1, TimeUnit.SECONDS);
            }

            T task = queue.remove();
            fullQueue.signalAll();
            return task;
        } finally {
            lock.unlock();
        }
    }
}
