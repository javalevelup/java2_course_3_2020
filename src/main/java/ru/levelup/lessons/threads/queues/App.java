package ru.levelup.lessons.threads.queues;

public class App {

    public static void main(String[] args) {
        // Queue<String> queue = new SynchronizedQueue<>(5);
        Queue<String> queue = new ReentrantQueue<>(5);

        ProducerThread p1 = new ProducerThread(1, 0, queue);
        ProducerThread p2 = new ProducerThread(2, 16, queue);
        ProducerThread p3 = new ProducerThread(3, 31, queue);
        ProducerThread p4 = new ProducerThread(4, 46, queue);
        ProducerThread p5 = new ProducerThread(5, 61, queue);

        ShutdownFlag flag = new ShutdownFlag();
        ConsumerThread c1 = new ConsumerThread(1, queue, flag);
        ConsumerThread c2 = new ConsumerThread(2, queue, flag);


        c1.start();
        c2.start();

        p1.start();
        p2.start();
        p3.start();
        p4.start();
        p5.start();

        try {
            Thread.sleep(2000);
            System.out.println("Завершаем работу consumers");
            flag.setShutdown(true);
        } catch (InterruptedException exc) {}

        // c1.shutdown();
        // c2.shutdown();

    }

}
