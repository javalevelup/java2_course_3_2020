package ru.levelup.lessons.threads.queues;

public class ShutdownFlag {

    private volatile boolean shutdown;

    public boolean isShutdown() {
        return shutdown;
    }

    public void setShutdown(boolean shutdown) {
        this.shutdown = shutdown;
    }

}
