package ru.levelup.lessons.threads.queues;

import lombok.SneakyThrows;

import java.util.LinkedList;


public class SynchronizedQueue<T> implements Queue<T> {

    private final LinkedList<T> queue;
    private final int size;

    private final Object emptyQueue = new Object();
    private final Object fullQueue = new Object();

    public SynchronizedQueue(int size) {
        this.size = size;
        this.queue = new LinkedList<>();
    }

    @Override
    @SneakyThrows
    public void put(T task) {
        synchronized (fullQueue) {
            while (size == queue.size()) {
                System.out.println(Thread.currentThread().getName() + " ушел в ожидание");
                fullQueue.wait();
                System.out.println(Thread.currentThread().getName() + " проснулся");
            }
        }

        synchronized (emptyQueue) {
            queue.addLast(task);
            emptyQueue.notifyAll();
        }
    }

    // t1 -> sq.take() -> WAITING state

    // cons1 -> take()
    // cons2 -> take();
    @Override
    @SneakyThrows
    public T take() {
        // synchronized (this) { emptyQueue.wait(); } -> IllegalMonitorStateException
        synchronized (emptyQueue) { // << emptyQueue
            // wait
            while (queue.isEmpty()) {
                System.out.println(Thread.currentThread().getName() + " ушел в ожидание");
                emptyQueue.wait(); //
                System.out.println(Thread.currentThread().getName() + " проснулся");
            }
        }

        synchronized (fullQueue) {
            T task = queue.remove(); // queue.pop(); queue.removeFirst();
            fullQueue.notifyAll();
            return task;
        }
    }

}
