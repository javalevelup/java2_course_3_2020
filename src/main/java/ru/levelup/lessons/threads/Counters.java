package ru.levelup.lessons.threads;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

// три вида блокировки
// biased - one CAS
// thin - CAS на каждое переключение
// full (fat) - Полное переключение потока (смена контекста)
@SuppressWarnings("ALL")
public class Counters {

    @SneakyThrows // позволяет не писать catch для checked exceptions
    public static void main(String[] args) {

        Counter counter = new Counter();
        ReentrantLockCounter reentrantLockCounter = new ReentrantLockCounter();
        // Counter counter2 = new Counter();

        Thread t1 = new Thread(new CounterWorker(reentrantLockCounter));
        Thread t2 = new Thread(new CounterWorker(reentrantLockCounter));
        Thread t3 = new Thread(new CounterWorker(reentrantLockCounter));

        t1.start();
        t2.start();
        t3.start();

        t1.join();
        t2.join();
        t3.join();

        // 32 - 41 - 34
        System.out.println("Counter value: " + reentrantLockCounter.getCounter());

        Account from = new Account(10, 43563.23);
        Account to = new Account(15, 5964.23);

        AccountService accountService = new AccountService();

        // obj(from, to)
        // obj(to, from)

        Thread thread1 = new Thread(new AccountTransferWorker(from, to, accountService, 4300));
        Thread thread2 = new Thread(new AccountTransferWorker(to, from, accountService, 5455));
        thread1.start();
        thread2.start();

    }

    @RequiredArgsConstructor
    static class CounterWorker implements Runnable {

        private final Counter counter;

        @Override
        public void run() {
            try {
                for (int i = 0; i < 15; i++) {
                    counter.increment();
                    Thread.sleep(150);
                }
            } catch (InterruptedException exc) {
                throw new RuntimeException(exc);
            }
        }
    }

    static class Counter {

        // long counter; 32xbit system -> counter = counter + 1; -> read, read, increment, write, write
        private int counter;

        private final Object mutex = new Object();

        // mutual exclusion
        public synchronized void synchronizedIncrement() {
            counter++; // read, increment, write
        }

        public void increment() {
            synchronized (mutex) {
                System.out.println(Thread.currentThread().getName() + " current value: " + getCounter());
                System.out.println(Thread.currentThread().getName() + " took the monitor");
                counter++; // read, increment, write
                System.out.println(Thread.currentThread().getName() + " released the monitor");
            }
        }

        public int getCounter() {
            synchronized (mutex) {
                return counter;
            }
        }

    }

    static class ReentrantLockCounter extends Counter {

        private ReentrantLock reentrantLock = new ReentrantLock();
        private int counter;

        public void increment() {
            reentrantLock.lock();
            try {
                counter++;
            } finally {
                reentrantLock.unlock();
            }
        }

        public int getCounter() {
            reentrantLock.lock();
            try {
                return counter;
            } finally {
                reentrantLock.lock();
            }
        }
    }

    static class NonBlockingCounter extends Counter {

        // CAS -> compare and swap
        // цикл
        //      val = readValue
        //      val2 = val + 1
        //      val3 = readValue
        //      if (val3 == val) -> writeValue(val2)
        //      else повторяем цикл

        private AtomicInteger counter = new AtomicInteger(0);

        @Override
        public void increment() {
            counter.incrementAndGet();
        }

        @Override
        public int getCounter() {
            return counter.get();
        }
    }

    @AllArgsConstructor
    static class AccountTransferWorker implements Runnable {
        private Account from;
        private Account to;
        private AccountService accountService;
        private double amount;

        @Override
        public void run() {
            accountService.transferMoney(from, to, amount);
        }
    }

    static class AccountService {

        // from1 -> to1
        // from2 -> to2
        // from3 -> to4
        // 15, 10 -> sync(15) sync(10)
        // 10, 15 -> sync(15) sync(10)
        void transferMoney(Account from, Account to, double amount) {
            Account first = from.id > to.id ? from : to;
            Account second = from.id > to.id ? to : from;
            synchronized (first) {
                synchronized (second) {
                    from.amount -= amount;
                    to.amount += amount;
                }
            }
        }
    }

    @Getter
    @Setter
    @AllArgsConstructor
    static class Account {
        private int id;
        private double amount;
    }

}
