package ru.levelup.lessons.threads;

import org.hibernate.SessionFactory;
import ru.levelup.musicians.library.hibernate.HibernateUtils;
import ru.levelup.musicians.library.repository.BankRepository;
import ru.levelup.musicians.library.repository.impl.HibernateBankRepository;

public class BankThreadApp {

    public static void main(String[] args) throws InterruptedException {

        SessionFactory factory = HibernateUtils.getFactory();
        BankRepository bankRepository = new HibernateBankRepository(factory);

        Thread thread = new Thread(new BankLoader(bankRepository));
        thread.start();

        // thread.stop(); << kill -9
        // for (int i = 0; i < 7; i++) {
        Thread.sleep(1000);
//        }

        thread.interrupt(); // просим поток остановиться

        // boolean thread.isInterrupted();
        // boolean Thread.interrupted();

        // void thread.interrupt();

    }

}
